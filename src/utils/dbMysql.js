import knex from "knex";
import dotenv from "dotenv";
dotenv.config();

const db = knex({
  client: 'mysql',
  connection: {
    host : process.env.DATABASE_IP_SERVER,
    port : process.env.DATABASE_PORT,
    user : process.env.DATABASE_USERNAME,
    password : process.env.DATABASE_PASSWORLD,
    database : process.env.DATABASE_NAME,
  }
});



export default db;