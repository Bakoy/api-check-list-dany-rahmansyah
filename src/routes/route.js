import {
  loginController,
  registerController,
} from "../controllers/authController.js";
import {
  createNewCheckList,
  deleteChecklistById,
  getAllCheckList,
} from "../controllers/checkListController.js";
import {
  createNewCheckListItemInCheckListId,
  deleteCheckListItemInCheckListIdByChecklistItemId,
  getAllCheckListItemByCheckListId,
  getCheckListItemInCheckListIdByChecklistItemId,
  renameCheckListItemInCheckListIdByChecklistItemId,
  updateStatusCheckListItemInCheckListIdByChecklistItemId
} from "../controllers/checkListItemController.js";
import { tokenMiddleware } from "../middlewares/tokenMiddleware.js";

export const route = (app) => {
  app.post("/login", loginController);
  app.post("/register", registerController);

  app.use(tokenMiddleware);

  app.get("/checklist", getAllCheckList);
  app.post("/checklist", createNewCheckList);
  app.delete("/checklist/:checklistId", deleteChecklistById);

  app.get("/checklist/:checklistId/item", getAllCheckListItemByCheckListId);
  app.post("/checklist/:checklistId/item", createNewCheckListItemInCheckListId);
  app.get("/checklist/:checklistId/item/:checkListItemId", getCheckListItemInCheckListIdByChecklistItemId);
  app.put("/checklist/:checklistId/item/:checkListItemId", updateStatusCheckListItemInCheckListIdByChecklistItemId);
  app.delete("/checklist/:checklistId/item/:checkListItemId",deleteCheckListItemInCheckListIdByChecklistItemId);
  app.put("/checklist/:checklistId/item/rename/:checkListItemId",renameCheckListItemInCheckListIdByChecklistItemId);
};
