import dotenv from "dotenv";
import express from "express";
import cors from "cors";
import { route } from "./routes/route.js";
import { errorMidleware } from "./middlewares/errorMiddleware.js";
import { pageNotFoundMiddleware } from "./middlewares/pageNotFoundMiddleware.js";
dotenv.config();

const app = express();

app.use(cors());
app.use(express.json());

route(app);

app.use(errorMidleware);
app.use(pageNotFoundMiddleware);


app.listen(process.env.PORT_API, () => {
    console.log(`Server Node Run On Port ${process.env.PORT_API}`);
})

export default app;