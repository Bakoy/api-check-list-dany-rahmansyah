import jwt from "jsonwebtoken";
import db from "../utils/dbMysql.js";

export const tokenMiddleware = (req,res,next) => {
    if (req.headers.authorization) {
        const {authorization} = req.headers;
        const [bearer, token] = authorization.split(" ");
        jwt.verify(token, process.env.SECRET_KEY, (err, decodedToken) => {
            if (err) {
                res.status(401).json({
                    metadata: {
                        code: 401,
                        message: "unauthorize"
                    }
                })
            } else {
              const validasiEmail = db('user').where({
                email: decodedToken.email
              })
              if (validasiEmail.length == 0) {
                return res.status(401).json({
                    metadata: {
                        code: 401,
                        message: "unauthorize"
                    }
                })
              } else {
                req.decodedToken = decodedToken
                next();
              }
            }
          });
    } else {
        res.status(401).json({
            metadata: {
                code: 401,
                message: "unauthorize"
            }
        })
    }
};