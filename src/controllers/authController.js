import jwt from "jsonwebtoken";
import db from "../utils/dbMysql.js";

export const loginController = async (req, res) => {
  try {
    const validasiUser = await db("user").where({
      username: req.body.username,
      password: req.body.password,
    });
    if (validasiUser == 0) {
      return res.status(400).json({
        metadata: {
          code: 400,
          message: "Email/Password Tidak Terdaftar",
        },
      });
    }
    const accessToken = jwt.sign(
      { username: req.body.username },
      process.env.SECRET_KEY,
      { expiresIn: "3h" }
    );

    res.status(200).json({
      statusCode: 200,
      message: "Proses view detail berhasil",
      errorMessage: null,
      data: {
        token: accessToken,
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const registerController = async (req, res) => {
  try {
    const validasiEmail = await db("user").where({
      email: req.body.email,
      username: req.body.username,
    });
    if (validasiEmail.length !== 0) {
      return res.status(400).json({
        metadata: {
          code: 400,
          message: "Email Sudah Terdaftar",
        },
      });
    }
    await db("user").insert({
      email: req.body.email,
      password: req.body.password,
      username: req.body.username,
    });
    return res.status(200).json({
      statusCode: 200,
      message: "Proses save berhasil",
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
