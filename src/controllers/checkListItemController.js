import db from "../utils/dbMysql.js";

export const getAllCheckListItemByCheckListId = async (req, res) => {
  try {
    const data = await db("checklist_item").where({
      id_check_list: req.params.checklistId,
    });
    res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil ditemukan",
      },
      response: {
        data,
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const createNewCheckListItemInCheckListId = async (req, res) => {
  try {
    await db("checklist_item").insert({
      id_check_list: req.params.checklistId,
      itemName: req.body.itemName,
    });
    res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil disimpan",
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const getCheckListItemInCheckListIdByChecklistItemId = async (
  req,
  res
) => {
  try {
    const data = await db("checklist_item").where({
      id_check_list: req.params.checklistId,
      id: req.params.checkListItemId,
    });
    res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil ditemukan",
      },
      response: {
        data,
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const updateStatusCheckListItemInCheckListIdByChecklistItemId = async (
  req,
  res
) => {
  try {
    const cekStatus = await db("checklist_item").where({
      id_check_list: req.params.checklistId,
      id: req.params.checkListItemId,
    });

    if (cekStatus === 0) {
      return res.status(400).json({
        metadata: {
          code: 400,
          message: "id tidak ditemukan ",
        },
      });
    }

    await db("checklist_item")
      .where({
        id_check_list: req.params.checklistId,
        id: req.params.checkListItemId,
      })
      .update({
        status: cekStatus[0].status == 0 ? 1 : 0,
      });

    return res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil diupdate",
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const deleteCheckListItemInCheckListIdByChecklistItemId = async (
  req,
  res
) => {
  try {
    const data = await db("checklist_item")
      .where({
        id_check_list: req.params.checklistId,
        id: req.params.checkListItemId,
      })
      .del();

    if (data === 0) {
      return res.status(400).json({
        metadata: {
          code: 400,
          message: "id tidak ditemukan atau data sudah di hapus",
        },
      });
    }
    res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil dihapus",
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const renameCheckListItemInCheckListIdByChecklistItemId = async (
  req,
  res
) => {
  try {
    const cekStatus = await db("checklist_item").where({
      id_check_list: req.params.checklistId,
      id: req.params.checkListItemId,
    });

    if (cekStatus === 0) {
      return res.status(400).json({
        metadata: {
          code: 400,
          message: "id tidak ditemukan",
        },
      });
    }

    await db("checklist_item")
      .where({
        id_check_list: req.params.checklistId,
        id: req.params.checkListItemId,
      })
      .update({
        itemName: req.body.itemName,
      });

    res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil diupdate",
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
