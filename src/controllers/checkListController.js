import db from "../utils/dbMysql.js";

export const getAllCheckList = async (req, res) => {
  try {
    const { username } = req.decodedToken;
    const data = await db("checklist").where({
      username,
    });
    res.status(200).json({
      metadata: {
        code: 200,
        message: "data berhasil ditemukan",
      },
      response: {
        data,
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const createNewCheckList = async (req, res) => {
  try {
    const { username } = req.decodedToken;
    await db("checklist").insert({
      username,
      name: req.body.name,
    });
    res.status(201).json({
      metadata: {
        code: 201,
        message: "Berhasil Input Data",
      },
      response: {
        data: req.body,
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
export const deleteChecklistById = async (req, res) => {
  try {
    const deleted = await db("checklist")
      .where({
        id: req.params.checklistId,
      })
      .del();
    if (deleted === 0) {
      return res.status(400).json({
        metadata: {
          code: 400,
          message: "id tidak ditemukan atau data sudah di hapus",
        },
      });
    }
    return res.status(200).json({
      metadata: {
        code: 200,
        message: "berhasil dibapus",
      },
    });
  } catch (error) {
    res.status(500).json({
      metadata: {
        code: 500,
        message: error?.message,
      },
    });
  }
};
